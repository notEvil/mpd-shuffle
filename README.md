# mpd-shuffle

This is my approach to the problem [ashuffle](https://github.com/joshkunz/ashuffle) solves. It is far from ashuffles level of sophistication and feature set but imo sometimes practicality beats purity/completeness. Python is much more hackable compared to C++, so if you want to try something different, you may want to start here.

The biggest difference to ashuffle is the possibility to change the list of songs to draw from on-the-fly. When a song with a certain URL (argument `--trigger`) gets added to the queue, this script will use the songs in the queue for future draws.

**Edit 2020-10**: Since the first version I've experimented with hard and softskip to lower the probability of certain songs (initially penalizing entire genres, then artists and now individual songs). In short, a pause followed by a skip within a few seconds is a hardskip and a skip in the first half of the song is a softskip. The weight assigned to each song is 1/(1 + #softskip + 10*#hardskip). If you have a better idea to balance music libraries with listening preference, please let me know!

Happy hacking :)
