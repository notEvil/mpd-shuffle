import mpd
import argparse
import collections
import itertools
import logging
import os
import os.path as o_path
import pickle
import random
import time


TEMPORARY_PLAYLIST = ".tmp.shuffle"


logging.basicConfig(
    level=os.environ.get("PYTHON_LOG_LEVEL", "WARNING"),
    format="%(asctime)s %(levelname)-8s %(message)s",
)


parser = argparse.ArgumentParser()

parser.add_argument(
    "--trigger",
    default="shuffle.wav",
    help="URL of song to trigger reshuffle; default: shuffle.wav",
)
parser.add_argument(
    "--persist",
    default="./.shuffle_songs",
    help='Path of file to use for persistency; use "" to disable persistency; default: ./.shuffle_songs',
)
parser.add_argument(
    "--unique", default="20", help="Number of unique songs to preselect; default: 20"
)
parser.add_argument(
    "--queue", default="1", help="Number of songs to prequeue; default: 1"
)
parser.add_argument(
    "--wait",
    default="0.5",
    help="Number of seconds to wait before suspend/resume; use 0 to disable suspend/resume; default: 0.5",
)
parser.add_argument(
    "--all",
    default="All",
    help='Name of playlist holding all songs to keep up to date; use "" to disable updating; default: All',
)
parser.add_argument(
    "--hardskip-path",
    default="./hard skipped",
    help='Path of file to log hard song skip; use "" to disable logging; default: "./hard skipped"',
)
parser.add_argument(
    "--hardskip-timeout",
    default="5",
    help="Number of seconds to log hard song skip; default: 5",
)
parser.add_argument(
    "--hardskip-factor",
    default="10",
    help="Inverse factor to scale song probability; default: 10",
)
parser.add_argument(
    "--softskip-path",
    default="./soft skipped",
    help='Path of file to log soft song skip; use "" to disable logging; default: "./soft skipped"',
)

arguments = parser.parse_args()


if arguments.persist == "":
    arguments.persist = None

else:
    arguments.persist = o_path.normpath(
        o_path.join(o_path.dirname(__file__), arguments.persist)
    )

arguments.unique = int(arguments.unique)
assert 0 < arguments.unique

arguments.queue = int(arguments.queue)
assert 0 <= arguments.queue

arguments.wait = float(arguments.wait)
if arguments.wait == 0.0:
    arguments.wait = None

else:
    assert 0 < arguments.wait

if arguments.all == "":
    arguments.all = None

if arguments.hardskip_path == "":
    arguments.hardskip_path = None

else:
    arguments.hardskip_path = o_path.normpath(
        o_path.join(o_path.dirname(__file__), arguments.hardskip_path)
    )

arguments.hardskip_timeout = float(arguments.hardskip_timeout)
assert 0 < arguments.hardskip_timeout

arguments.hardskip_factor = float(arguments.hardskip_factor)
assert 0 < arguments.hardskip_factor

arguments.hardskip_path = o_path.normpath(
    o_path.join(o_path.dirname(__file__), arguments.hardskip_path)
)

if arguments.softskip_path == "":
    arguments.softskip_path = None

else:
    arguments.softskip_path = o_path.normpath(
        o_path.join(o_path.dirname(__file__), arguments.softskip_path)
    )


class SongPool:
    def __init__(self, n_unique, hardskip_counts, softskip_counts):
        self.n_unique = n_unique
        self.hardskip_counts = hardskip_counts
        self.softskip_counts = softskip_counts

        self._songs = []
        self._queue = collections.deque()

        self._cumulative_weights = []

    def __len__(self):
        result = len(self._songs)
        return result

    def update_songs(self, songs):
        self._songs.clear()
        self._songs.extend(self._get_song(song) for song in songs)
        self.update_probabilities()

    def _get_song(self, song):
        result = dict(file=song["file"])
        return result

    def draw_song(self):
        result = self._queue.popleft()
        self._enqueue()
        return result

    def save(self, path):
        with open(path, "wb") as file:
            pickle.dump(self._songs, file)

    def load(self, path):
        with open(path, "rb") as file:
            songs = pickle.load(file)

        self.update_songs(songs)

    def update_probabilities(self):
        self._cumulative_weights.clear()
        self._cumulative_weights.extend(
            itertools.accumulate(self._get_weight(song) for song in self._songs)
        )

        self._queue.clear()
        for _ in range(min(len(self._songs), self.n_unique)):
            self._enqueue()

    def _get_weight(self, song):
        result = 1
        hardskip_count = self.hardskip_counts.get(song["file"], None)
        if hardskip_count is not None:
            result /= hardskip_count * arguments.hardskip_factor
        softskip_count = self.softskip_counts.get(song["file"], None)
        if softskip_count is not None:
            result /= softskip_count + 1
        return result

    def _enqueue(self):
        while True:
            (song,) = random.choices(self._songs, cum_weights=self._cumulative_weights)

            if song not in self._queue:
                break

        self._queue.append(song)


def get_status(client):
    status = client.status()
    status["playlistlength"] = int(status["playlistlength"])
    song = status.get("song", None)
    if song is not None:
        status["song"] = int(song)
    elapsed = status.get("elapsed", None)
    if elapsed is not None:
        status["elapsed"] = float(elapsed)
    return status


def parse_song(song):
    song["duration"] = float(song["duration"])


def enqueue(n, song_pool, client):
    if len(song_pool) == 0:
        return

    for _ in range(n):
        client.add(song_pool.draw_song()["file"])


def softskip(song, softskip_counts, path):
    logging.info("soft skip")

    with open(path, "a") as file:
        file.write(song["file"])
        file.write("\n")

    softskip_counts[song["file"]] = softskip_counts.get(song["file"], 0) + 1


client = mpd.MPDClient()
suspended = False
pause_song_position = None
pause_time = None
hardskip_counts = {}
play_song_position = None
play_song = None
play_time = None
softskip_counts = {}

if arguments.hardskip_path is not None and o_path.exists(arguments.hardskip_path):
    with open(arguments.hardskip_path, "r") as file:
        for line in file:
            song_url = line[:-1]
            hardskip_counts[song_url] = hardskip_counts.get(song_url, 0) + 1

if arguments.softskip_path is not None and o_path.exists(arguments.softskip_path):
    with open(arguments.softskip_path, "r") as file:
        for line in file:
            song_url = line[:-1]
            softskip_counts[song_url] = softskip_counts.get(song_url, 0) + 1

song_pool = SongPool(arguments.unique, hardskip_counts, softskip_counts)

if arguments.persist is not None and o_path.exists(arguments.persist):
    song_pool.load(arguments.persist)


client.connect("localhost", 6600)

try:
    while True:
        subsystems = client.idle("playlist", "player", "database")

        if "playlist" in subsystems:
            status = get_status(client)

            if status["playlistlength"] == 0:
                if arguments.wait is not None:
                    time.sleep(arguments.wait)

                    status = get_status(client)

                    if status["playlistlength"] == 0:  # playlist is still empty
                        logging.info("resume")

                        suspended = False
                        enqueue(arguments.queue + 1, song_pool, client)
                        client.play()

                    else:  # playlist got filled
                        logging.info("suspend")

                        suspended = True

            else:
                (song,) = client.playlistinfo(status["playlistlength"] - 1)
                if song["file"] == arguments.trigger:  # shuffle triggered
                    logging.info("shuffle")

                    client.deleteid(song["id"])

                    song_pool.update_songs(client.playlistinfo())
                    suspended = False

                    client.clear()
                    enqueue(arguments.queue + 1, song_pool, client)
                    client.play()

                    if arguments.persist is not None:
                        song_pool.save(arguments.persist)

        if "player" in subsystems:
            status = get_status(client)

            if (status["state"] == "stop" and "song" not in status.keys()) or status[
                "state"
            ] == "play":  # end of playlist
                if not suspended:
                    logging.info("enqueue")

                    enqueue(
                        max(
                            0,
                            arguments.queue
                            + status.get("song", status["playlistlength"])
                            - status["playlistlength"]
                            + 1,
                        ),
                        song_pool,
                        client,
                    )
                    if status["state"] != "play" and status["playlistlength"] != 0:
                        client.play(status.get("song", status["playlistlength"]))

            if arguments.hardskip_path is not None:
                if status["state"] == "pause":
                    pause_song_position = status["song"]
                    pause_time = time.monotonic()

                elif status["state"] == "play" and pause_song_position is not None:
                    if (
                        status["song"] == (pause_song_position + 1)
                        and (time.monotonic() - pause_time)
                        <= arguments.hardskip_timeout
                    ):
                        logging.info("hard skip")

                        (song,) = client.playlistinfo(pause_song_position)
                        with open(arguments.hardskip_path, "a") as file:
                            file.write(song["file"])
                            file.write("\n")

                        hardskip_counts[song["file"]] = (
                            hardskip_counts.get(song["file"], 0) + 1
                        )

                        if arguments.softskip_path is not None:
                            softskip(song, softskip_counts, arguments.softskip_path)

                        song_pool.update_probabilities()

                    pause_song_position = None
                    pause_time = None

            if arguments.softskip_path is not None and not suspended:
                if status["state"] == "play":
                    if (
                        play_song_position is not None
                        and status["song"] == (play_song_position + 1)
                        and (time.monotonic() - play_time)
                        <= (play_song["duration"] * 0.5)
                    ):
                        softskip(play_song, softskip_counts, arguments.softskip_path)
                        song_pool.update_probabilities()

                    play_song_position = status["song"]
                    (play_song,) = client.playlistinfo(play_song_position)
                    parse_song(play_song)
                    play_time = time.monotonic() - status["elapsed"]

                else:
                    play_song_position = None
                    play_song = None
                    play_time = None

        if arguments.all is not None:
            if "database" in subsystems:
                client.save(TEMPORARY_PLAYLIST)
                client.clear()
                client.findadd("(!(file == '{}'))".format(arguments.trigger))
                client.rm(arguments.all)
                client.save(arguments.all)
                client.clear()
                client.load(TEMPORARY_PLAYLIST)
                client.rm(TEMPORARY_PLAYLIST)

finally:
    client.close()
    client.disconnect()
